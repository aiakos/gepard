from typing import TypeVar, List, Union
from uuid import UUID

from .core import Entity, Collection, ManyToOne, OneToMany, plural_name, EntityDoesNotExist, MultipleEntitiesReturned

T = TypeVar('T')


def sql_table_name(Entity):
	return Label(getattr(Entity, 'sql_table_name', None) or plural_name(Entity))


def sql(val):
	try:
		return val.sql
	except AttributeError:
		if isinstance(val, int):
			return str(val)
		if isinstance(val, UUID):
			return f"'{str(val)}'"
		if isinstance(val, str):
			return f"""'{val.replace("'", "''")}'"""

class Alias:
	def __init__(self, name, definition):
		self.name = name
		self.definition = definition

	@property
	def sql(self):
		return f'{self.definition.sql} AS {self.name.sql}'

class BinaryOp:
	def __init__(self, arg1, op, arg2):
		self.arg1 = arg1
		self.op = op
		self.arg2 = arg2

	@property
	def sql(self):
		return f'{sql(self.arg1)} {self.op} {sql(self.arg2)}'

class Label:
	def __init__(self, value):
		self.value = value

	def __getitem__(self, field):
		return Field(self, field)

	@property
	def sql(self):
		return f'"{self.value}"'

class LeftJoin:
	def __init__(self, source, on):
		self.source = source
		self.on = on

	@property
	def sql(self):
		return f'LEFT JOIN {self.source.sql} ON {self.on.sql}'

class InnerJoin:
	def __init__(self, source, on):
		self.source = source
		self.on = on

	@property
	def sql(self):
		return f'INNER JOIN {self.source.sql} ON {self.on.sql}'

class Field:
	def __init__(self, table, field):
		self.table = table
		self.field = Label(field) if isinstance(field, str) else field

	@property
	def sql(self):
		return f'{self.table.sql}.{self.field.sql}'

class Star:
	sql = '*'

class Query:
	def __init__(self, source=None, joins=None, where=None, select=None):
		self.source = source
		self.joins = joins or []
		self.where = where or []
		self.select = select or []

	@property
	def sql(self):
		return '\n'.join([
			'SELECT ' + ', '.join(x.sql for x in self.select),
			'FROM ' + self.source.sql,
			*(x.sql for x in self.joins),
			*(['WHERE ' + ' AND '.join(x.sql for x in self.where)] if len(self.where) else []),
		])

	def __repr__(self):
		return self.sql


def _make_query(self: Union[Entity, Collection], anons=None):
	schema = self if isinstance(self, type) else type(self)

	anons = anons or (Label(f'_t{i}') for i in range(1, 10000))
	this = next(anons)
	query = Query(
		source = Alias(this, sql_table_name(schema)),
	)

	for (field, op, value) in self.where:
		rel = getattr(schema, field, None)

		if rel:
			subquery = _make_query(value, anons)

			if isinstance(rel, ManyToOne) and op == '=':
				one = subquery.source.name
				query.joins.append(InnerJoin(subquery.source, BinaryOp(this[rel.name + '_id'], '=', one['id'])))
			elif isinstance(rel, OneToMany) and op == 'contain':
				any = subquery.source.name
				query.joins.append(InnerJoin(subquery.source, BinaryOp(this['id'], '=', any[rel.reverse_name + '_id'])))

			query.joins += subquery.joins
			query.where += subquery.where

		elif isinstance(value, Entity):
			print("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tOOH WE GOT A STRANGE ENTITY!", field, op, value)

		else:
			query.where.append(BinaryOp(this[field], op, value))

	return query


async def _read_rows(cursor, Row):
	column_numbers = {c.name: i for i, c in enumerate(cursor.description)}
	async for raw in cursor:
		row = Row()
		for k, t in Row.__annotations__.items():
			setattr(row, k, raw[column_numbers[k]])
		yield row


class SQLEntity(Entity):

	def __init_subclass__(cls, **kwargs):
		super().__init_subclass__(**kwargs)
		class Row:
			def __repr__(self):
				return repr(self.__dict__)

			@property
			def __json__(self):
				return self.__dict__

		Row.__annotations__ = {}
		for k, t in cls.Data.__annotations__.items():
			if issubclass(t, str) or issubclass(t, UUID) or issubclass(t, int):
				Row.__annotations__[k] = t
			if issubclass(t, Entity):
				Row.__annotations__[k + '_id'] = t.Row.__annotations__['id']
		cls.Row = Row

	@classmethod
	async def create(cls: T, data) -> T:
		...

	@classmethod
	async def list(cls: T) -> List[T]:
		query = _make_query(cls)
		query.select.append(query.source.name[Star()])

		async with cls.db.cursor() as cur:
			await cur.execute(str(query))

			async for row in _read_rows(cur, cls.Row):
				yield row

	async def get(self) -> dict:
		query = _make_query(self)
		query.select.append(query.source.name[Star()])

		async with self.db.cursor() as cur:
			await cur.execute(str(query))

			the_row = None
			async for row in _read_rows(cur, self.Row):
				if the_row:
					raise MultipleEntitiesReturned()
				else:
					the_row = row
			if not the_row:
				raise EntityDoesNotExist()
			return the_row

	async def put(self, data):
		...

	async def patch(self, data):
		...

	async def delete(self):
		...
