from uuid import UUID


class TextType(type):
	def __getitem__(self, max_length):
		class LimitedText(str):
			pass
		LimitedText.max_length = max_length
		return LimitedText


class Text(str, metaclass=TextType):
	pass
