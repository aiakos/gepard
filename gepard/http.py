from aiohttp import web
from urllib.parse import unquote
from functools import reduce
from inspect import signature

from .json import dumps

def iscallable(func, num_args=0):
	try:
		sig = signature(func)
	except TypeError:
		return False

	if len(sig.parameters) != num_args:
		return False

	return True

def ListHandler(inner_handler):
	async def handle():
		return [x async for x in inner_handler()]
	return handle

async def return_dict():
	return {}

def methods(node):
	return {k: v for k, v in {
		'OPTIONS': return_dict,
		'GET': node.get if iscallable(getattr(node, 'get', None)) else ListHandler(node.list) if iscallable(getattr(node, 'list', None)) else None,
	}.items() if v is not None}

apibrowser = """<!DOCTYPE html><html><head><meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1"><title>APIBrowser</title><link href=https://apibrowser.storage.googleapis.com/static/css/app.568f5941d42567ef2a65c5cdb7b32463.css rel=stylesheet integrity="sha256-wu9idIWIq6GpemTy2UjJyALKwWzhx3asHU+SG2udyEE=" crossorigin=anonymous></head><body><div id=app></div><script type=text/javascript src=https://apibrowser.storage.googleapis.com/static/js/app.9f8edfd9b0e1afb81473.js integrity="sha256-Sdgc+hP8zDdwQwDYypvPePGGs3m7Auqw9Qs9uxOwCoQ=" crossorigin=anonymous></script></body></html>"""

async def handle_request(root, request):
	if 'text/html' in request.headers['accept']:
		return web.Response(text=apibrowser, content_type='text/html', headers=dict(Vary='Accept'))

	edges = [unquote(edge) for edge in request.raw_path.split('/') if edge]
	try:
		node = reduce(lambda node, edge: node[edge], edges, root)
	except KeyError:
		raise web.HTTPNotFound()

	supported_methods = methods(node)
	if request.method not in supported_methods:
		raise web.HTTPMethodNotAllowed(request.method, supported_methods.keys())

	handle = supported_methods[request.method]
	resp = await handle()
	return web.Response(body=dumps(resp).encode('utf-8'), content_type='application/json', headers=dict(Vary='Accept', Allow=', '.join(supported_methods.keys())))
