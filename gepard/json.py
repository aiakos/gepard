from uuid import UUID
import json

def jsonable(o):
	if hasattr(o, '__json__'):
		return o.__json__
	if isinstance(o, UUID):
		return str(o)
	return o


def dumps(obj):
	return json.dumps(obj, default=jsonable)
