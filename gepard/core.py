from itertools import chain


def plural_name(Entity):
	return getattr(Entity, 'plural_name', None) or Entity.__name__.lower() + 's'


class MultipleEntitiesReturned(Exception):
	pass


class EntityDoesNotExist(Exception):
	pass


class Collection(type):
	where = []

	def __repr__(self):
		return f'<{self.__name__}Set: {self.where}>'

	def __getitem__(self, edge):
		return self(edge)


def primary_key(Entity):
	return tuple(getattr(Entity, 'primary_key', None) or ('id',))


def natural_key(Entity):
	return tuple(getattr(Entity, 'natural_key', None) or primary_key(Entity))


class Entity(metaclass=Collection):
	def __init__(self, natural_value = None, where = None, db = None, **kwargs):
		if db:
			self.db = db

		where = type(self).where + (where or [])

		for (k, v) in kwargs.items():
			if '__' in k:
				field, op = k.split('__')
				where.append(field, op, v)
			else:
				where.append(k, '=', v)

		if natural_value is not None:
			fields = natural_key(type(self))
			set_fields = {field for field, op, val in where}
			for field in fields:
				if field not in set_fields:
					natural_value_type = type(self).Data.__annotations__[field]
					value = natural_value if isinstance(natural_value, natural_value_type) else natural_value_type(natural_value)
					if isinstance(value, Entity) and db:
						value.db = db
					where.append((field, '=', value))

		self.where = where

	def __repr__(self):
		return f'<{type(self).__name__}: {self.where}>'

	def __getitem__(self, edge):
		if isinstance(getattr(type(self), edge, None), Relation):
			return getattr(self, edge)
		else:
			raise KeyError(edge)


class SingleEntity(Entity):
	def __init__(self):
		super().__init__(None)

	def __repr__(self):
		return f'<{type(self).__name__}>'


class Relation:
	def __init__(self, to, reverse=None):
		self.to = to
		self.reverse_name = reverse


class OneToMany(Relation):
	def __set_name__(self, owner, name):
		self.name = name
		if not self.reverse_name:
			self.reverse_name = owner.__name__.lower()
		return self

	def __get__(self, instance, owner):
		if instance == None:
			return self

		class collection(self.to):
			pass
		collection.__name__ = self.to.__name__
		collection.__qualname__ = self.to.__qualname__
		collection.where = [
			(self.reverse_name, '=', instance)
		]
		if getattr(instance, 'db', None):
			collection.db = instance.db
		return collection


class ManyToOne(Relation):
	def __set_name__(self, owner, name):
		self.name = name
		if not self.reverse_name:
			self.reverse_name = plural_name(owner)
		return self

	def __get__(self, instance, owner):
		if instance == None:
			return self

		return self.to(where=[
			(self.reverse_name, 'contain', instance),
		], db=getattr(instance, 'db', None))
