from gepard.core import SingleEntity, OneToMany, ManyToOne
from gepard.sql import SQLEntity
from gepard.data import UUID, Text


# CREATE TABLE apps ( id varchar(50) );
# INSERT INTO apps VALUES ('shop');
# INSERT INTO apps VALUES ('calendar');
class App(SQLEntity):
	class Data:
		id: Text[50]


# CREATE TABLE users ( id varchar(50) );
# INSERT INTO users VALUES ('lew21');
class User(SQLEntity):
	class Data:
		id: Text[50]


# CREATE TABLE app_users ( app_id varchar(50), user_id varchar(50), primary key (app_id, user_id) );
# INSERT INTO app_users VALUES ('shop', 'lew21');
class AppUser(SQLEntity):
	plural_name = 'app_users'
	primary_key = ['app', 'user']

	app = ManyToOne(App)
	user = ManyToOne(User)

	class Data:
		app: App
		user: User


User.apps = OneToMany(AppUser).__set_name__(User, 'apps')
App.users = OneToMany(AppUser).__set_name__(App, 'users')


class Root(SingleEntity):
	def __init__(self, db=None):
		self.db = db

	users = OneToMany(User)
	apps = OneToMany(App)


root = Root()

assert(isinstance(root.users('lew21').apps('shop'), AppUser))
assert(isinstance(root.apps('shop').users('lew21'), AppUser))

print(root.users('lew21').apps('shop'))
print(root.apps('shop').users('lew21'))

import asyncio
import aiopg

async def main():
	pool = await aiopg.create_pool('postgres://root@172.17.0.2:26257/gepard')
	async with pool.acquire() as conn:
		root = Root(conn)

		print(root.users('lew21').apps('shop').where)
		print(await root.users('lew21').apps('shop').get())
		print()

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
