from gepard.core import SingleEntity, OneToMany, ManyToOne
from gepard.sql import SQLEntity
from gepard.data import UUID, Text
from gepard.http import handle_request

"""
CREATE TABLE accounts ( id uuid primary key, first_name varchar(100), last_name varchar(100) );
INSERT INTO accounts VALUES ('bcc3aae7-242f-4009-b38f-00077396e5be', 'Linus', 'Lewandowski');
CREATE TABLE identities ( id varchar(200), account_id uuid references accounts (id), trusted bool );
INSERT INTO identities VALUES ('linus@lew21.net', 'bcc3aae7-242f-4009-b38f-00077396e5be', true);
"""

class Account(SQLEntity):
	plural_name = 'accounts'
	#identities = OneToMany(Identity)

	class Data:
		id: UUID
		first_name: Text[100]
		last_name: Text[100]


class Identity(SQLEntity):
	plural_name = 'identities'
	account = ManyToOne(Account)

	class Data:
		id: Text[200]
		#sub: Text[100]
		#domain: Text[100]
		account: Account
		trusted: bool

		@property
		def sub(self):
			return self.id.split('@')[0]

		@property
		def domain(self):
			return self.id.split('@')[1]


Account.identities = OneToMany(Identity).__set_name__(Account, 'identities')
Identity.account = ManyToOne(Account).__set_name__(Identity, 'account')


class Root(SingleEntity):
	def __init__(self, db=None):
		self.db = db

	accounts = OneToMany(Account)


root = Root()
print(root)
print(root.accounts)

lew21 = root.accounts(UUID('bcc3aae7-242f-4009-b38f-00077396e5be'))
print(lew21)
print(lew21.identities)

linus_at_lew21_net = lew21.identities('linus@lew21.net')
print(linus_at_lew21_net)
print()

print(Identity('linus@lew21.net').account)
print()
print()
print()

import asyncio
import aiopg
from aiohttp import web

async def main(loop):
	pool = await aiopg.create_pool('postgres://root@172.17.0.2:26257/gepard')
	async with pool.acquire() as conn:
		root = Root(conn)

		lew21 = root.accounts(UUID('bcc3aae7-242f-4009-b38f-00077396e5be'))
		linus_at_lew21_net = lew21.identities('linus@lew21.net')

		print(linus_at_lew21_net.where)
		print(await linus_at_lew21_net.get())
		print()

		print(lew21.identities.where)
		async for row in lew21.identities.list():
			print(row)
		print()

	async def app(request):
		async with pool.acquire() as conn:
			root = Root(conn)
			return await handle_request(root, request)

	server = web.Server(app)
	await loop.create_server(server, "127.0.0.1", 8080)
	print("======= Serving on http://127.0.0.1:8080/ ======")

	# pause here forever by serving HTTP requests and
	# waiting for keyboard interruption
	while True:
		await asyncio.sleep(3600)

loop = asyncio.get_event_loop()

try:
    loop.run_until_complete(main(loop))
except KeyboardInterrupt:
    pass

loop.close()
